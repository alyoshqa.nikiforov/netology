# Project for studying

## Learning stages

- Version control systems for DevOps engineer :heavy_check_mark:
- Virtualization and containerization :heavy_check_mark:
- cloud infrastructure. Terraform :heavy_check_mark:
- Configuration Management System :heavy_check_mark:
- Continuous development and integration :triangular_flag_on_post:
- Monitoring and logs :watch:
- Microservices :watch:
- Kubernetes: basics, application and administration :watch:
- Organization of the project with the help of cloud providers :watch:
- Diploma block of the profession DevOps-engineer :watch:

## Contacts with me

<a href="https://t.me/AlexeyNikiforov"><img src="resource/Telegram.svg" width="40"></a>
<a href="mailto:alyoshqa.nikiforov@gmail.com"><img src="resource/Gmail.svg" width="40"></a>

## My repo

<a href="https://github.com/A1yoshQa/netology"><img src="resource/Github.svg" width="40"></a>
<a href="https://gitlab.com/alyoshqa.nikiforov/netology"><img src="resource/Gitlab.svg" width="40"></a>
